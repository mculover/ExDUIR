#pragma once
#include "help_ex.h"

#define  GROUPBOX_TEXT_OFFSET		0		//定义GroupBox中的Text相对于左边的偏移
#define  GROUPBOX_RADIUS			1		//定义GroupBox中的线框圆角度
#define  GROUPBOX_STROKEWIDTH		2		//定义GroupBox中的线的宽度

void _groupbox_paint(EXHANDLE hObj, obj_s* pObj);
size_t CALLBACK _groupbox_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam);

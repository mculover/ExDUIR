#pragma once
#include "help_ex.h"

size_t CALLBACK _menubutton_proc(HWND hWnd, EXHANDLE hObj, UINT uMsg, size_t wParam, size_t lParam);
void _menubutton_paint(EXHANDLE hObj, obj_s* pObj);
size_t CALLBACK _menubutton_menu_proc(HWND hWnd, EXHANDLE hExDUI, UINT uMsg, size_t wParam, size_t lParam, int* lpResult);
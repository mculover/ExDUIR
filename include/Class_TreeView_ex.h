#pragma once

struct EX_TREEVIEW_NODEITEM {
	size_t nID; // 0
	LPCWSTR lpTitle; // 4
	size_t lParam; // 8
	size_t nImageIndex; // 12
	size_t nImageIndexExpand; // 16
    BOOL fExpand; // 20
	size_t dwStyle; // 24
	size_t nDepth; // 28
	EX_TREEVIEW_NODEITEM* pParent; // 32
	EX_TREEVIEW_NODEITEM* pPrev; // 36
	EX_TREEVIEW_NODEITEM* pNext; // 40
	EX_TREEVIEW_NODEITEM* pChildFirst; // 44
	int nCountChild; // 48
};

void _tv_register();


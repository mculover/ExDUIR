#pragma once
#include "help_ex.h"

//======================================================
// 函数名称：_imglist_draw
// 返回类型：逻辑型
// 参数<1>：hImageList
// 参数<2>：nIndex
// 参数<3>：hCanvas
// 参数<4>：nLeft
// 参数<5>：nTop
// 参数<6>：nRight
// 参数<7>：nBottom
// 参数<8>：nAlpha
//======================================================
BOOL _imglist_draw(void* hImageList, int nIndex, int hCanvas, int nLeft, int nTop, int nRight, int nBottom, int nAlpha);

//======================================================
// 函数名称：_imglist_count
// 返回类型：整数型
// 参数<1>：hImageList
//======================================================
int _imglist_count(void* hImageList);

BOOL _imglist_del(void* hImageList, size_t nIndex);

BOOL _imglist_set(void* hImageList, int nIndex, void* pImg, int dwBytes);

BOOL _imglist_destroy(void* hImageList);

size_t _imglist_add(void* hImageList, void* pImg, SIZE_T dwBytes, int nIndex);

BOOL _imglist_setimage(void* hImageList, int nIndex, EXHANDLE hImg);

int _imglist_size(void* hImageList, int* pWidth, int* pHeight);

size_t _imglist_addimage(void* hImageList, int hImg, int nIndex);

void* _imglist_create(int width, int height);

size_t _imglist_get(void* hImageList, size_t nIndex);
#pragma once
#include "help_ex.h"

#define PNG_HEADER 1196314761
#define PNG_IHDR 1380206665
#define PNG_PLTE 1163152464
#define PNG_tRNS 1397641844
#define PNG_acTL 1280598881
#define PNG_fcTL 1280598886
#define PNG_IDAT 1413563465
#define PNG_fdAT 1413571686

#define IMGF_APNG 1

#pragma pack(1)
struct EX_APNG_THUNK {
	DWORD dwLen;
	DWORD Type;
	UINT sequence_number;// 序列
	UINT width;// 宽度
	UINT height;// 高度
	UINT x_offset;// 水平偏移
	UINT y_offset;// 垂直偏移
	USHORT delay_num;// 为这一帧显示时间的以秒为单位的分子
	USHORT delay_den;// 为这一帧显示时间以秒为单位的分母
	char dispose_op;// 处理方式
	char blend_op;// 混合模式
};
#pragma pack()

struct lockedbitmapdata_s
{
	int width_;
	int height_;
	int stride_;
	int pixelformat_;
	void* scan0_;
	int dwlen_;
	void* pLock_;
};

struct img_s
{
	int dwFlags_;
	void* pObj_;
	int nMaxFrames_;
	int nCurFrame_;
	void* pWicDecoder_;
	EX_APNG_THUNK** lpFrames_;
	void* lpHeader_;
	void* pPrev_;
	int p_x_;
	int p_y_;
	int p_w_;
	int p_h_;
};

struct bkgimg_s
{
	int dwFlags_;
	EXHANDLE hImage_;
	int x_;
	int y_;
	int dwRepeat_;
	void* lpGrid_;
	int* lpDelay_;
	int curFrame_;
	int maxFrame_;
	int dwAlpha_;
};

EXHANDLE _wic_create(int width, int height, GUID pFormat, int* nError);
EXHANDLE _wic_init_from_decoder(void* pDecoder, int* nError);
void _wic_savetobin(void* pBitmap, void** lpBin, size_t* len, int* nError);
void _wic_drawframe(img_s* pImg, void* pFrame, int* nError, D2D1_RECT_F* dest = NULL);
void* _wic_convert(void* pBitmap, bool bFreeOld, int* nError);
void* _wic_selectactiveframe(void* pDecoder, int nIndex, int* nError, D2D1_RECT_F* dest);
void* _wic_getpixel(void* pBitmap, int x, int y, int* nError);
bool _wic_getframedelay(void* pDecoder, int* lpDelay, int nCount, int* nError);

bool _img_destroy(EXHANDLE hImg);
void* _img_getpixel(EXHANDLE hImg, int x, int y);
bool _img_lock(EXHANDLE hImg, void* lpRectL, int flags, lockedbitmapdata_s* lpLockedBitmapData);
bool _img_unlock(EXHANDLE hImg, lockedbitmapdata_s* lpLockedBitmapData);
bool _img_setpixel(EXHANDLE hImg, int x, int y, int color);
bool _img_getsize(EXHANDLE hImg, void* lpWidth, void* lpHeight);
int _img_width(EXHANDLE hImg);
int _img_height(EXHANDLE hImg);
EXHANDLE _img_create(int width, int height);
EXHANDLE _img_createfrompngbits(void* lpmem);
void* _img_createfromstream_init(void* lpData, int dwLen, int* nError);
EXHANDLE _img_createfromstream(void* lpStream);
EXHANDLE _img_createfrommemory(__in void* lpData, __in int dwLen, __out_opt EXHANDLE* phImg);
EXHANDLE _img_createfromhicon(__in void* hIcon, __out_opt EXHANDLE* phImg);
EXHANDLE _img_createfromfile(__in LPCWSTR lpwzFilename, __out_opt EXHANDLE* phImg);
EXHANDLE _img_init(void* pObj, int curframe, int frames, void* pDecoder, int* nError);
EXHANDLE _img_copyrect(EXHANDLE hImg, int x, int y, int width, int height);
EXHANDLE _img_copy(EXHANDLE hImg);
EXHANDLE _img_scale(EXHANDLE hImage, int dstWidth, int dstHeight);
size_t _img_savetomemory(EXHANDLE hImage, void* lpBuffer);
bool _img_getframedelay(EXHANDLE hImg, int* lpDelayAry, int nFrames);
int _img_getframecount(EXHANDLE hImage);
void* _img_getcontext(EXHANDLE hImage);
void _apng_drawframe(img_s* pImage, int nIndex);
int _apng_thunk_getlength(void* lpMem);
bool _apng_thunk_getnext(void* lpMem, int* nPos, int dwThunkType, EX_APNG_THUNK** lpThunk, int* dwThunkLen);
void _apng_int(EXHANDLE hImage, void* lpStream);
bool _apng_getframedelay(img_s* pImg, int* lpDelay, int nFrames);
bool _img_selectactiveframe(EXHANDLE hImg, int nIndex);
EXHANDLE _img_createfromres(hashtable_s* hRes, int atomPath);
EXHANDLE _img_createfromhbitmap(void* hBitmap, void* hPalette, int fPreAlpha);
void _img_savetofile(EXHANDLE hImage, LPCWSTR wzFileName);
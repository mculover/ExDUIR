#include "Brush_ex.h"
void* _brush_create(int argb)
{
	D2D1_COLOR_F color = {};
	ARGB2ColorF(argb, &color);
	void* hBrush = nullptr;
	((ID2D1DeviceContext*)g_Ri.pD2DDeviceContext)->CreateSolidColorBrush(color, (ID2D1SolidColorBrush**)&hBrush);
	return hBrush;
}

int _brush_destroy(void* hBrush)
{
	int nError = 0;
	if (hBrush != 0)
	{
		nError = ((ID2D1SolidColorBrush*)hBrush)->Release();
	}
	return nError;
}

void _brush_setcolor(void* hBrush, int argb)
{
	D2D1_COLOR_F color = {};
	ARGB2ColorF(argb, &color);
	((ID2D1SolidColorBrush*)hBrush)->SetColor(color);
}

void* _brush_createfromimg(EXHANDLE hImg)
{
	img_s* pImg = nullptr;
	int nError = 0;
	ID2D1BitmapBrush* hBrush = nullptr;
	if (_handle_validate(hImg, HT_IMAGE, (void**)&pImg, &nError))
	{
		void* pObj = pImg->pObj_;
		ID2D1Bitmap* pBitmap = nullptr;
		auto ret = ((ID2D1DeviceContext*)g_Ri.pD2DDeviceContext)->CreateBitmapFromWicBitmap((IWICBitmapSource*)pObj, &pBitmap);
		if (ret == 0)
		{
			D2D1_BITMAP_BRUSH_PROPERTIES pro2 = {};

			pro2.extendModeX = D2D1_EXTEND_MODE_WRAP;
			pro2.extendModeY = D2D1_EXTEND_MODE_WRAP;
			pro2.interpolationMode = D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR;

			nError = ((ID2D1DeviceContext*)g_Ri.pD2DDeviceContext)->CreateBitmapBrush(pBitmap, pro2, &hBrush);
		}
		pBitmap->Release();
	}
	return (void*)hBrush;
}

void* _brush_createfromcanvas2(EXHANDLE hCanvas, int alpha) {
	canvas_s* pCanvas = nullptr;
	int nError = 0;
	ID2D1BitmapBrush* hBrush = nullptr;
	if (_handle_validate(hCanvas, HT_CANVAS, (void**)&pCanvas, &nError))
	{
		void* pContext = _cv_context(pCanvas);
		D2D1_BITMAP_BRUSH_PROPERTIES pro2 = { };
		D2D1_BRUSH_PROPERTIES pro = { 0 };
		pro.opacity = (float)alpha / 255;
		pro.transform.m11 = 1.0;
		pro.transform.m22 = 1.0;
		pro2.extendModeX = D2D1_EXTEND_MODE_WRAP;
		pro2.extendModeY = D2D1_EXTEND_MODE_WRAP;
		pro2.interpolationMode = D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR;
		nError = ((ID2D1DeviceContext*)pContext)->CreateBitmapBrush((ID2D1Bitmap*)_cv_dx_bmp(pCanvas), pro2, pro, &hBrush);
	}
	return (void*)hBrush;
}

void* _brush_createfromcanvas(EXHANDLE hCanvas)
{
	canvas_s* pCanvas = nullptr;
	int nError = 0;
	ID2D1BitmapBrush* hBrush = nullptr;
	if (_handle_validate(hCanvas, HT_CANVAS, (void**)&pCanvas, &nError))
	{
		void* pContext = _cv_context(pCanvas);
		D2D1_BITMAP_BRUSH_PROPERTIES pro2 = {};

		pro2.extendModeX = D2D1_EXTEND_MODE_WRAP;
		pro2.extendModeY = D2D1_EXTEND_MODE_WRAP;
		pro2.interpolationMode = D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR;
		nError = ((ID2D1DeviceContext*)pContext)->CreateBitmapBrush((ID2D1Bitmap*)_cv_dx_bmp(pCanvas), pro2, &hBrush);
	}
	return (void*)hBrush;
}

void _brush_settransform(void* hBrush, matrix_s* matrix)
{
	((ID2D1BitmapBrush*)hBrush)->SetTransform(*(D2D1_MATRIX_3X2_F*)matrix);
}

HEXBRUSH _brush_createlinear_ex(FLOAT xs, FLOAT ys, FLOAT xe, FLOAT ye, const EXARGB* colors, const FLOAT* offsets, INT cStopPts)
{
	if (cStopPts < 2)
		return NULL;
	ID2D1GradientStopCollection* collection = nullptr;
	ID2D1LinearGradientBrush* hBrush = nullptr;
	D2D1_GRADIENT_STOP* stops = (D2D1_GRADIENT_STOP*)malloc(cStopPts * sizeof(D2D1_GRADIENT_STOP));
	D2D1_LINEAR_GRADIENT_BRUSH_PROPERTIES gradientProperties{};
	if (stops)
	{
		for (UINT i = 0; i < cStopPts; i++)
		{
			ARGB2ColorF(colors[i], &stops[i].color);
			stops[i].position = offsets[i];
		}

		((ID2D1DeviceContext*)g_Ri.pD2DDeviceContext)->CreateGradientStopCollection(stops, cStopPts, D2D1_GAMMA_2_2, D2D1_EXTEND_MODE_CLAMP, &collection);

		gradientProperties.startPoint.x = xs;
		gradientProperties.startPoint.y = ys;
		gradientProperties.endPoint.x = xe;
		gradientProperties.endPoint.y = ye;
		if (collection)
			((ID2D1DeviceContext*)g_Ri.pD2DDeviceContext)->CreateLinearGradientBrush(&gradientProperties, NULL, collection, &hBrush);

		free(stops);
		return hBrush;
	}
	return NULL;
}

HEXBRUSH _brush_createlinear(FLOAT xs, FLOAT ys, FLOAT xe, FLOAT ye, EXARGB crBegin, EXARGB crEnd)
{
	EXARGB colors[] = { crBegin, crEnd };
	FLOAT offsets[] = { 0.0f, 1.0f };
	return _brush_createlinear_ex(xs, ys, xe, ye, colors, offsets, 2);
}